const path = require('path');

const cssLoaderOptions = JSON.stringify({
    modules: true,
    localIdentName: '[path][name]_[local]_[hash:base64:5]'
});

const tsLoaderOptions = JSON.stringify({
    configFileName: './src/tsconfig.json'
});

module.exports = {
    entry: "./src/index.ts",
    output: {
        filename: "app.js",
        path: __dirname + "/dist"
    },

    // Enable sourcemaps for debugging webpack's output.
    devtool: "source-map",

    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: ["", ".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
    },

    module: {
        loaders: [
            // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
            { 
                test: /\.tsx?$/, 
                loader: `awesome-typescript-loader?${tsLoaderOptions}`
            }, 
            {
                test: /\.scss$/,
                loaders: ["style-loader", `css-loader?${cssLoaderOptions}`, "sass-loader"]
            }
        ],

        preLoaders: [
            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
            { test: /\.js$/, loader: "source-map-loader" }
        ]
    } 
};