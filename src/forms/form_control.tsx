import * as React from 'react';
import { IObservableArray} from 'mobx';
import { state, _debounce } from '../core';
import { Validator } from './validation';

export interface IFormControl<T> {
	validationErrors: string[];
	isValidating: boolean;
	untouched: boolean;
	value: T;
}

export type Controls<T> = {
	[K in keyof T]: IFormControl<T[K]>;
}

export interface IFormControlProps<T> {
	validators?: Validator<T>[];
	name: string;
	required?: boolean;
	placeholder?: T;
}

export class FormControl<T, P extends IFormControlProps<T>> extends React.Component<P, any> implements IFormControl<T> {
	static contextTypes = {
	  form: React.PropTypes.object
	};

	@state validationErrors: IObservableArray<string> = [] as IObservableArray<string>;
	@state isValidating: boolean = false;
	@state untouched: boolean = true;
	@state value: T;

	private validationEtag: number = 0;

	//subclasses change this to customize their validation delay
	protected validationDelay: number = 500; 
	protected extractValue = null;
	protected default: () => T = () => null;

	componentWillMount(){
		if (this.value === undefined) {
			//first time being initialized
			this.value = this.default()
		}

		this.context.form 
			&& this.context.form.registerControl 
			&& this.context.form.registerControl(this);
	}

	componentWillUnmount(){
		this.context.form 
			&& this.context.form.unregisterControl 
			&& this.context.form.unregisterControl(this);
	}

	clear = () => {
		this.value = this.default();
	}

	//pass this and value to native components to make them 
	//controlled inputs
	onChange = (e) => {
		// get the new value
		const val = this.extractValue ? this.extractValue(e) : e.target.value;

		if (this.untouched) {
			this.untouched = false;
		}

		this.value = val;

		// run validations
		this.runValidations();
	}

	runValidations = _debounce(async () => {
		const validators = this.props.validators;

		if (!this.value || !validators || !validators.length) {
			this.validationErrors.clear();
			return;
		}

		this.validationEtag++;
		const etag = this.validationEtag;

		this.isValidating = true;

		const validations = validators.map(validate => validate(this.value));

		const validationResults = await Promise.all(validations);

		if (etag === this.validationEtag) {

			const validationErrors = validationResults.filter(result => !result.isValid).map(result => result.reason);
			
			this.validationErrors.replace(validationErrors);

			this.isValidating = false;
		}
	}, this.validationDelay);
}