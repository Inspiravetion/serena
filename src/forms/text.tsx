import * as React from 'react';
import { IStyleProps } from '../core';

interface ITextProps extends IStyleProps {
	center?: boolean;
}

export const Text = (props: ITextProps) => {
	const textStyle = {
		marginTop: '0',
		marginBottom: '0',
		textAlign: props.center ? 'center' : 'initial' 
	};

	const className = `ms-Panel-headerText ${props.className || ''}`

	return (
		<span {...props} className={className} style={Object.assign({}, textStyle, props.style || {})}>
			{props.children}
		</span>
	);
}

export const LargeText = (props: ITextProps) =>
	<Text {...props} style={Object.assign({}, props.style, { fontSize: '42px'})}/>

export const SmallText = (props: ITextProps) =>
	<Text {...props} style={Object.assign({}, props.style, { fontSize: '14px'})}/>