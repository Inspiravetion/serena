export type ValidationResult = {
	isValid: boolean;
	reason?: string;
}

export type Validator<T> = (value: T) => Promise<ValidationResult>;