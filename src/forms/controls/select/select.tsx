import * as React from 'react';
import { state, derived_state, controller, asMap, ObservableMap, IStyleProps, ClickEvent } from '../../../core';
import {RelativeContainer, Container, VerticalContainer,CenteredContent, Section, VerticalSection} from '../../../layouts/container';
import { Text } from '../../text';
import { FormControl, IFormControlProps } from '../..';

import {
  Button,
  ButtonType
} from 'office-ui-fabric-react';

export interface ISelectOption<T> { 
  id: string,
  label: string, 
  value: T
}

export interface ISelectProps<T> extends IStyleProps, IFormControlProps<T[]> {
  options: ISelectOption<T>[];
  value?: ISelectOption<T>;
  defaultMessage?: string;
  multiSelect?: boolean;
  onSelect?: (targetItem: ISelectOption<T>, selectedItems: T[]) => void;
  onUnSelect?: (targetItem: ISelectOption<T>, selectedItems: T[]) => void;
  onReplaceSelect?: (unselectedItem: ISelectOption<T>, selectedItem: T) => void;
  visibleOptionCount?: number;
}

const selectContainerStyle = {
  position: 'relative'
}

const selectStyle = {
  height: '36px',
  backgroundColor: 'white',
  border: '1px solid #ccc'
}

const optionsListStyle = {
    backgroundColor: 'white',
    borderLeft: '1px #ccc solid',
    borderRight: '1px #ccc solid',
    zIndex: 100,
    position: 'absolute',
    width: '100%',
    height: 'initial',
    boxShadow: 'rgba(0, 0, 0, 0.1) 0px 5px 15px -3px',
    top: '36px'
}

const optionsStyle = {
    backgroundColor: 'white',
    borderBottom: '1px #ccc solid',
    padding: '8px', 
    height: '36px',
    width: '100%'
}

const selectedOptionsStyle = Object.assign({}, optionsStyle, {
    backgroundColor: '#eee'
})

const selectDisplayContentStyle = {
  padding: '8px',
  textOverflow: 'ellipsis',
  overflow: 'hidden',
  whiteSpace: 'nowrap'
}

const selectDisplayTextStyle = Object.assign({}, selectDisplayContentStyle, {
  opacity: '.5'
})

const displayContentStyle = {
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap'
}

@controller()
export class Select<T> extends FormControl<T[], ISelectProps<T>> {
  @state isOpen = false;
  @state selectedOptions: ObservableMap<ISelectOption<T>> = asMap<ISelectOption<T>>({});
  
  default = () => [];

  extractValue = (options: ISelectOption<T>[]): T[] => options.map(opt => opt.value);

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    super.componentWillMount();
    // clicking outside of the input closes it
    window.addEventListener('click', this.forceClose);

    const selectedOptions = this.value;
    const options = this.props.options;

    //if theres already a value set by an existing form
    if (selectedOptions && selectedOptions.length) {
      //select the corresponding options
      selectedOptions.forEach(selectedOpt => {
        // NOTE*** assumes option values are mutually exclusive
        const opt = options.find(option => option.value === selectedOpt);

        if (opt) {
          this.selectedOptions.set(opt.id, opt);
        }
      });
    } else if (!selectedOptions || !selectedOptions.length && this.props.value) {
      //if there aren't any selected options (null or [])
      const opt = this.props.value;

      if (opt) {
        this.selectedOptions.set(opt.id, opt);
        this.value = this.extractValue(this.selectedOptions.values());
      }
    }
  }

  componentWillUnMount() {
    super.componentWillUnmount();
    window.removeEventListener('click', this.forceClose);
  }

  toggleOpen() {
    if (this.props.options.length > 1) {  
      this.isOpen = !this.isOpen;
    }
  }

  forceClose = () => this.isOpen = false;

  onDropdownClicked = (e: ClickEvent) => {
    e.stopPropagation();
    this.toggleOpen();
  }

  onOptionClicked = (option: ISelectOption<T>) => (e: ClickEvent) => {
    e.stopPropagation();

    //unselect previous if in single select mode and you aren't previous
    if (!this.props.multiSelect && this.selectedOptions.size && !this.selectedOptions.get(option.id)) {
      let toReplace = this.selectedOptions.values()[0];

      this.selectedOptions.clear();
      this.selectedOptions.set(option.id, option);
      //call on change here
      this.onChange(this.selectedOptions.values());
      this.props.onReplaceSelect && this.props.onReplaceSelect(toReplace, option.value);
      
      this.toggleOpen();
      return;
    } 

    //already selected
    if (this.selectedOptions.get(option.id)) {
      //unselect current
      this.selectedOptions.delete(option.id);
      this.onChange(this.selectedOptions.values());
      this.props.onUnSelect && this.props.onUnSelect(option, this.selectedOptions.values().map(val => val.value));
      
      if (!this.props.multiSelect) {
        this.toggleOpen();
      }
      return;
    }

    //select current
    this.selectedOptions.set(option.id, option);
    this.onChange(this.selectedOptions.values());
    this.props.onSelect && this.props.onSelect(option, this.selectedOptions.values().map(val => val.value));

    if (!this.props.multiSelect) {
      this.toggleOpen();
    }
  }

  widget(options) {
    return (
      <VerticalContainer style={{ position: 'relative' }}>
          <VerticalSection style={{ position: 'relative' }}>  
            <Container onClick={this.onDropdownClicked}
                       style={Object.assign({}, selectContainerStyle, selectStyle, this.props.style || {})}>
              <Section>
                <VerticalContainer center>
                  { 
                    this.selectedOptions.size 
                      ? <span style={selectDisplayContentStyle}>
                          { this.selectedOptions.values().map(opt => opt.label).join(', ') }
                        </span>
                      : <span style={selectDisplayTextStyle}>
                          { this.props.defaultMessage || '' }
                        </span>
                  }
                </VerticalContainer>
              </Section>
              <VerticalContainer center style={{ float: 'right', height: '36px', width: '36px' }}>
                <Button style={{ color: this.props.style && this.props.style.color || null }}
                        buttonType={ ButtonType.icon }
                        icon='ChevronDown'/>
              </VerticalContainer>
            </Container>
          </VerticalSection>  

          {
            this.isOpen && this.props.options
              ? <VerticalSection style={this.props.visibleOptionCount 
                                               ? Object.assign({}, optionsListStyle, { overflowY: 'scroll', maxHeight: `${this.props.visibleOptionCount * 36}px` }) 
                                               : optionsListStyle}>
                  <VerticalContainer>
                    {
                      this.props.options.map(opt =>
                        <VerticalSection onClick={this.onOptionClicked(opt)}>
                          <VerticalContainer center>
                            <span style={this.selectedOptions.get(opt.id) ? selectedOptionsStyle : optionsStyle}>
                              { opt.label }
                            </span>
                          </VerticalContainer>
                        </VerticalSection>
                      )
                    }
                  </VerticalContainer>
                </VerticalSection>
              : null
          }
      </VerticalContainer>
    );
  }

  renderLoading() {
    return this.widget([]);
  }

  render() {
    return this.widget(this.props.options);
  }
}