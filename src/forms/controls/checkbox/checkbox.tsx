import * as React from 'react';
import { FormControl, IFormControlProps } from '../..';
import { controller } from '../../../core';

const classnames = require('classnames/bind');
const styles = classnames.bind(require("./checkbox.scss"));

export interface CheckBoxProperties extends IFormControlProps<boolean> {}

@controller()
export class CheckBox extends FormControl<boolean, CheckBoxProperties> {
	
	default = () => false;

	extractValue = (e: React.ChangeEvent<any>): boolean => e.target.checked

	renderLoading() {
		return null;
	}

	render() {
		const classname = styles('checkbox-base');

		return (
			<input type='checkbox' 
				   className={classname}
				   onChange={this.onChange}
				   checked={this.value}
				   {...this.props as any}/>
		);
	}
}