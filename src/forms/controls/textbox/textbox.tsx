import * as React from 'react';
import { FormControl, IFormControlProps } from '../..';
import { controller, IStyleProps, state } from '../../../core';
import { Container, Section } from '../../../layouts/container';

const classnames = require('classnames/bind');
const styles = classnames.bind(require("./textbox.scss"));

export interface TextBoxProperties extends IFormControlProps<string> {}

@controller()
export class TextBox extends FormControl<string, TextBoxProperties> {
	
    default = () => '';

	renderLoading() {
		return null;
	}	

	render() {
		const classname = styles('textbox-base');

		return (
			<input type='text' 
				   className={classname} 
				   onChange={this.onChange}
				   value={this.value}
				   {...this.props as any}/>
		);
	}
}

export interface DecoratedTextBoxProperties extends IStyleProps, IFormControlProps<string> {
	leftIcon?: JSX.Element,
	rightIcon?: JSX.Element, 
	inputStyle?: React.CSSProperties 
}

@controller()
export class DecoratedTextBox extends FormControl<string, DecoratedTextBoxProperties> {
	@state focused: boolean = false;

	onFocus = (e) => {
		e.stopPropagation();
		this.focused = true;
	}

	onBlur = (e) => {
		e.stopPropagation();
		this.focused = false;
	}

	render() {
		const containerClassname = styles('decorated-textbox', 'container', { focused: this.focused });
		const inputClassname = styles('input');
		const leftIconClassname = styles('leftIcon');
		const rightIconClassname = styles('rightIcon');

		const { leftIcon, rightIcon, style, inputStyle, ...inputProps } = this.props;

		return (
			<Container className={containerClassname} style={this.props.style}>
				{	
					leftIcon
						? <div className={leftIconClassname}>
							{ leftIcon }
						</div>
						: null
				}
				<Section>
					<input type='text' 
					   className={inputClassname}
					   style={inputStyle} 
					   onChange={this.onChange}
					   onFocus={this.onFocus}
					   onBlur={this.onBlur}
					   value={this.value}
					   {...inputProps as any}/>
				</Section>	
				{	
					rightIcon
						? <div className={rightIconClassname}> 
							{ rightIcon }
						</div>
						: null
				}
			</Container>
		);
	}
}