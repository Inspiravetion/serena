import * as React from 'react';
import {IStyleProps} from '../../../core';

const classnames = require('classnames/bind');
const styles = classnames.bind(require("./button.scss"));

export interface ButtonProperties extends IStyleProps {}

export class Button extends React.Component<ButtonProperties, any> {
	renderLoading() {
		return null;
	}	

	render() {
		const classname = styles('button-base');

		return (
			<button className={classname} {...this.props}/>
		);
	}
}