import * as React from 'react';
import { state, derived_state, controller, autorun, _debounce, asMap, ObservableMap, IStyleProps, ClickEvent } from '../core';
import { Controls, IFormControl} from './form_control';

export interface IForm<T> {
	controls: Controls<T>;
	value: T;
	isValid: boolean;
	isValidating: boolean;
	clear();
	registerControl<K extends keyof T>(control: IFormControl<T[K]>);
	unregisterControl<K extends keyof T>(control: IFormControl<T[K]>);
}

export class Form<T> extends React.Component<any, any> implements IForm<T> {
	static childContextTypes = {
	  form: React.PropTypes.object
	};

	private disposers = {};

	@state _value = {} as T;
	@state _controls: Controls<T> = asMap({}) as Controls<T>;

	@derived_state get value() : T {
		//gets rid of mobx properties
		return Object.assign({}, this._value);
	}

	@derived_state get isValid() : boolean {
		return !this._controls.values().some(control => {
			return control.validationErrors.length || (control.props.required && !control.value)
		});
	}

	@derived_state get isValidating() : boolean {
		return this._controls.values().some(control => control.isValidating);
	}

	controls: Controls<T> = new Proxy({}, {
		get: (target, name) => {
			return this._controls.get(name);
		}
	});

	getChildContext() {
	    return {
	    	form : {
	    		registerControl: this.registerControl,
	    		unregisterControl: this.unregisterControl
	    	}
	    };
	  }

	clear() {
		this._controls.values().forEach(control => control.clear());
	}

	clearExcluding(controls: string[]) {
		const controlKeys = this._controls.keys().filter(key => controls.indexOf(key) === -1);
		
		controlKeys.forEach(controlKey => {
			const control = this._controls.get(controlKey);
			control.clear();
		})
	}

	registerControl = <K extends keyof T>(control: IFormControl<T[K]>) => {
		const key: K = control.props.name;

		// *** this means form consumers can't return undefined in default() ***
		const existingState = this._value[key] !== undefined;

		this._controls.set(key, control);
		
		// if the form already has a previous value for the control this is a remount within the form
		if (existingState) {
			control.value = this._value[key];
		} else {
			this._value[key] = control.value;
		}

		this.disposers[key as string] = autorun(() => this._value[key] = control.value);
	}

	unregisterControl = <K extends keyof T>(control: IFormControl<T[K]>) => {
		const key: K = control.props.name;
		this._controls.delete(key);
		// this._value[key] = null; // not sure if we should nullify it...more like just dispose of 

		const dispose =	this.disposers[key as string];
		if (dispose) {
			dispose();
			this.disposers[key as string] = null;
		}

	}
}