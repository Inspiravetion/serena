import { mount, ConstructorFunction, TypedConstructorFunction, IMiddleware } from './engine';
import {Component} from 'react';


// NOTE: this can't handle abstract classes
export function extendConstructor(target : ConstructorFunction, newFunctionality : (proto: ConstructorFunction, args: any[]) => void) : ConstructorFunction {
    // subclass the target to inherit it's functionality  
    return class extends target {
    	constructor(...args) {
    		super(...args);

    		// get access to its 'this' after the base constructor runs 
    		// and pass new functionality the prototype so metadata put on by decorators 
    		// is accessable
    		newFunctionality.apply(this, [target.prototype, args]);
    	}
    };
}

export function classAndPropertyDecorator(classDecorator: ClassDecorator, propertyDecorator: PropertyDecorator): ClassDecorator & PropertyDecorator {
	return function () {
		const args = Array.prototype.slice.apply(arguments).filter(arg => arg !== undefined);

		if (args.length === 1) {
			return classDecorator.apply(null, args);
		} else {
			return propertyDecorator.apply(null, args);
		}
	}
}

export function propertyDecorator<T>(decoratorKey: string | symbol, transform?: (key: string) => T, sideEffects?: (target, key) => void) {
	return function (target, key) {
		if (!target[decoratorKey]) {
			target[decoratorKey] = [];
		}

		target[decoratorKey].push(transform ? transform(key) : key);
		sideEffects && sideEffects(target, key);
	}
}

export function parameterizedPropertyDecorator<T>(decoratorKey: string | symbol, transform?: (key: string, ...args: any[]) => T) {
	return function(...args) {
		return function (target, key) {
			if (!target[decoratorKey]) {
				target[decoratorKey] = [];
			}

			target[decoratorKey].push(transform ? transform(key, ...args) : key);
		}
	}
}

export function parameterizedClassDecorator<T>(decoratorKey: string | symbol, transform?: (...args: any[]) => T){
	return function(...args: any[]) {
		return function(target: new (...args: any[]) => Component<any, any>) {
			let proto = target.prototype;

			if (!proto[decoratorKey]) {
				proto[decoratorKey] = transform ? transform(...args) : args;
			} else {
				throw `Can't use class decorators more than once on the same class.`;
			}
		}
	}
}

export const middlewareDecorator = (middlewareConstructors: (new () => IMiddleware)[]) => (target, key) => {
	const middlewareProviders = middlewareConstructors.map(Middleware => () => new Middleware());
	mount(...middlewareProviders)(target, key);		
}

export const parameterizedMiddlewareDecorator = (middlewareConstructors: (new (...args) => IMiddleware)[]) => (...args) => (target, key) => {
	const middlewareProviders = middlewareConstructors.map(Middleware => () => new Middleware(...args));
	mount(...middlewareProviders)(target, key);		
}