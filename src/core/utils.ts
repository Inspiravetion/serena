export type SortResult = 0 | 1 | -1;

export type SortFunction<T> = (prev: T, next: T) => SortResult;

export function sortByKey<T>(key?: keyof T) : SortFunction<T> { 
  return function(a: T, b: T) : SortResult {
    if (!a || !b) throw 'UNSORTABLE';

    let valA = key ? a[key] : a,
        valB = key ? b[key] : b;

    if (valA < valB) {
      return -1;
    } else if (valA > valB) {
      return 1;
    } 

    return 0;
  }
}

export function sortByTransform<T, U>(transform?: (T) => U) : SortFunction<T> { 
  return function(a: T, b: T) : SortResult {
    if (!a || !b) throw 'UNSORTABLE';

    let valA = transform ? transform(a) : a,
        valB = transform ? transform(b) : b;

    if (valA < valB) {
      return -1;
    } else if (valA > valB) {
      return 1;
    } 

    return 0;
  }
}

export const sleep = (timeMs) => new Promise(resolve => setTimeout(resolve, timeMs));
