import * as React from 'react';

export * from './controller';
export * from './http';
export * from './engine';
export * from './boot';
export * from './extensions';
export * from './inject';
export * from './utils';

/**
 * start looking into React.HtmlProps etc
 */
export interface IReactProps {
  children?: React.ReactNode 
  onClick?: ClickHandler
}
  
export type ClickHandler = React.MouseEventHandler<any>
export type ClickEvent = React.MouseEvent<any>;

export interface IStyleProps extends IReactProps {
  style?: React.CSSProperties;
  className?: string;
}