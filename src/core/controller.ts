import {Component} from 'react';

export interface IController extends Component<any, any> {
    renderLoading(): JSX.Element;
}

export class Controller extends Component<any, any> implements IController {
	public isLoading: boolean;
	
	renderLoading(): JSX.Element {
		throw 'All controllers must implement renderLoading';
	}
}