
export type Headers = { [key: string] : string };

export enum Method {
	GET, 
	POST, 
	PUT, 
	DELETE
}

export interface IHttpOptions {
	url: string
	headers?: Headers,
	body?: string
}

export interface IHttpFullOptions extends IHttpOptions {
	method: Method
}

export interface IHttp  {
	get<T>(options: IHttpOptions): Promise<T>;
	post<T>(options: IHttpOptions): Promise<T>;
	put<T>(options: IHttpOptions): Promise<T>;
	delete<T>(options: IHttpOptions): Promise<T>;
	getJSON<T>(options: IHttpOptions): Promise<T>;
	postJSON<T>(options: IHttpOptions): Promise<T>;
	putJSON<T>(options: IHttpOptions): Promise<T>;
	deleteJSON<T>(options: IHttpOptions): Promise<T>;
}

export class Http implements IHttp {

	get<T>(options: IHttpOptions): Promise<T> {
		return this.request(options.url, Object.assign(options, { method: Method.GET, body: null }));
	}

	post<T>(options: IHttpOptions): Promise<T> { 
		return this.request(options.url, Object.assign(options, { method: Method.POST }));
	}

	put<T>(options: IHttpOptions): Promise<T> { 
		return this.request(options.url, Object.assign(options, { method: Method.PUT }));
	}

	delete<T>(options: IHttpOptions): Promise<T> { 
		return this.request(options.url, Object.assign(options, { method: Method.DELETE }));
	}

	getJSON<T>(options: IHttpOptions): Promise<T> {
		return this.get(Object.assign(options, { 
			headers: { 
				'Content-Type' : 'application/json;charset=UTF-8' 
			}, body: null 
		}));
	}

	postJSON<T>(options: IHttpOptions): Promise<T> { 
		return this.post(Object.assign(options, { 
			headers: { 
				'Content-Type' : 'application/json;charset=UTF-8' 
			} 
		}));
	}

	putJSON<T>(options: IHttpOptions): Promise<T> { 
		return this.put(Object.assign(options, { 
			headers: { 
				'Content-Type' : 'application/json;charset=UTF-8' 
			} 
		}));
	}

	deleteJSON<T>(options: IHttpOptions): Promise<T> { 
		return this.delete(Object.assign(options, { 
			headers: { 
				'Content-Type' : 'application/json;charset=UTF-8' 
			} 
		}));
	}

	async request<T>(url: string, options: IHttpFullOptions): Promise<T> {
		const opts = {
			method : Method[options.method], 
			body : options.body,
			headers : options.headers
		};

		const res = await fetch(url, opts);

		return res.json();
	}
}

// interface IWebSocketService {
// 	on<T>(eventName: string, cb: (T) => void);
// 	emit<T>(eventName: string, eventData: T);
// }

// //might require build steps le sigh
// interface IWebWorkerService {}

// interface Result<S, E> {}

// //check if action is a promise...if so await on it
// type Action = () => void;
// type Test<T> = (T) => boolean;
// type Transform<In, Out> = (In) => Out;

// class AsyncResult<T> extends Promise<T> {
// 	constructor(executor: (resolve: (value?: T | PromiseLike<T>) => void, reject: (reason?: any) => void) => void) {
// 		super(executor);
// 	}

// 	// on resolve
// 	and(action: Action): AsyncResult<T> { return this }
// 	// on reject
// 	or(action: Action): AsyncResult<T> { return this }

// 	// result agnostic modifiers
// 	map<Out>(transform: Transform<T, Out>): AsyncResult<Out> { return <any> this }
// 	if(test: Test<T>, action?: Action): AsyncResult<T> { return this }
// 	unless(test: Test<T>): AsyncResult<T> { return this }
	
// 	// result specific modifiers
// 	and_if(test: Test<T>, action?: Action): AsyncResult<T> { return this }
// 	or_if(test: Test<T>, action?: Action): AsyncResult<T> { return this }
	
// 	and_map<Out>(transform: Transform<T, Out>): AsyncResult<Out> { return <any> this }
// 	or_map<Out>(transform: Transform<T, Out>): AsyncResult<Out> { return <any> this }

// 	// back to promise for promise parity (await etc.)
// 	please(action?: Action): Promise<T> { return null }
// }

// async function main() {

// 	let res = new AsyncResult((resolve, reject) => {
// 		resolve(2);
// 	})
// 		// on resolve ... .and(action) === .and_if(() => true, action)
// 		.and_if(num => num % 2 === 0, () => console.log('even'))
// 		// on reject ... .or(action) === .or_if(() => true, action)
// 		.or_if(statusCode => statusCode > 400)
// 			.and_if(err => err === 503, () => console.log('failed and it is because a servic is down'))
// 			.and_if(err => err === 500, () => console.log('internal server error'))
// 			.or_if(err => err === 301)
// 				.do(() => console.log('301'))
// 		// optional action...if supplied, it is used for the last flow modifier...ie or_if here
// 		.please(() => console.log('or_if worked'));

// 	console.log(res);
// }

// main().then(() => console.log('done'));
