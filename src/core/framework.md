/blackmagic
	/store - base store impl X
	/actions - base actions impl X
	/model - base model impl X
	/controller - base controller impl X 
	/middleware - middleware that ships with the framework X 
	/observers - observers that ships with the framework X
	/boot - boot func (register stores, controllers, services) X
	/service - base service impls (WebWorker, WS, Http etc.) X
	/core - decorators used everywhere + types X
	/extensions - functions to create extension points into the framework like action modifiers and such X


	/*
store - state management
  model - pure state (derivable)
  actions - unpure state requiring runtime calculation/api calls
view - ui controls
  controller - map statless views to models and actions
  stateless view - construct visual interface
middleware - power to change execution path of actions
observers - power to observe actions and model updates
services - in charge of data fetching

 */