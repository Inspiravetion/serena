import "reflect-metadata";

import {createElement, ComponentClass} from 'react';
import {render} from 'react-dom';

import {STORE_KERNEL, KERNEL} from './engine';

export interface Mapping {
	id: string;
	impl: new (...args) => any;
}

export type BootConfig = {
	stores: Mapping[], 
	services: Mapping[], 
	controllers: Mapping[]
};

export function init(stores: Mapping[], services: Mapping[], controllers: Mapping[]) {
	stores.forEach(store => {
		STORE_KERNEL.bind(store.id).to(store.impl).inSingletonScope()
	});

	services.forEach(service => 
		KERNEL.bind(service.id).to(service.impl).inSingletonScope()
	);

	controllers.forEach(controller => 
		KERNEL.bind(controller.id).toConstructor(controller.impl)
	);
}


/**
 * import { boot, BootConfig } from 'serena/core'
 *
 * import { UserStore, InventoryStore } from './stores'
 * import { HttpService, LogService } from './services'
 * import { Dashboard, UserDetails, ProductDetails } from './controllers'
 * 
 * const bootConfig = {
 *     stores: [ 
 *         { 
 *             id: 'users', 
 *             impl: UserStore 
 *         }, 
 *         { 
 *             id: 'inventory', 
 *             impl: InventoryStore 
 *         } 
 *     ],
 *     services: [
 *         {
 *             id: 'http',
 *             impl: HttpService
 *         },
 *         {
 *             id: 'log',
 *             impl: LogService
 *         }
 *     ],
 *     controllers: [
 *         {
 *             id: 'user',
 *             impl: UserDetails
 *         },
 *         {
 *             id: 'product',
 *             impl: ProductDetails
 *         }
 *     ]
 * }
 * 
 * boot(bootConfig, Dashboard, 'appRoot');
 * 
 * @param config DI configuration for controllers, stores, and services
 * @param rootComponent the Class for the root controller of the app
 * @param rootElementId the DOM id of the element to attach to
 */
export function boot(config: BootConfig, rootComponent: ComponentClass<any>, rootElementId: string) {
	init(config.stores, config.services, config.controllers);
	render(createElement(rootComponent), document.getElementById(rootElementId));
}