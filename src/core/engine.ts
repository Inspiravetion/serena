import {Kernel, inject, injectable, decorate} from 'inversify';
import {observer as mobxObserver} from 'mobx-react';
import {when, observe, computed, untracked, asStructure, observable, extendObservable, IObservableValue, transaction, autorun, toJS, action as mobxAction} from 'mobx';
import {Component} from 'react';
import {classAndPropertyDecorator, parameterizedClassDecorator, propertyDecorator, parameterizedMiddlewareDecorator, parameterizedPropertyDecorator, extendConstructor, middlewareDecorator} from './extensions';

export * from 'mobx';
export * from 'inversify';

export interface IAny {};
export type ConstructorFunction = new(...args) => IAny;
export type TypedConstructorFunction<T> = new(...args: any[]) => T;

export interface IFrameworkEvent {
	id: string;
	storeName: string;
	timestamp: string;
}

export interface IActionEvent extends IFrameworkEvent {
  actionName: string;
  args: any[];
}

export interface IModelEvent extends IFrameworkEvent {
  propertyName: string;
  from: any;
  to: any;
  derived: boolean;
}

export type ActionMiddleWareHandler = (abort: (reason?: any) => void, event: IActionEvent) => Promise<void>;
export type EventHandler<T> = (event: T) => void;
export type ActionEventHandler = EventHandler<IActionEvent>;
export type ModelEventHandler = EventHandler<IModelEvent>;
export type Handler = () => void;
export interface IMiddleware {
  isLoading: boolean;

  /**
   * 1) Action started but no middleware have run, allows for action call specific initialization of a middleware
   */
  start?: ActionEventHandler;

  /**
   * 2) Middleware that decides whether an action should be called 
   */
  exec?: ActionMiddleWareHandler;

  /**
   * 3) Respond to a middleware aborting that ran after you
   */
  abort?: ActionEventHandler;

  /**
   * 4) Middleware finished and Sync Mutator is about to run
   */
  beforeMutator?: ActionEventHandler;

  /**
   * 5) Middldeware finished and Async action is about to to run
   */
  beforeAsync?: ActionEventHandler;

  /**
   * 6) Async action finished and async  mutator middleware are about to be run
   */
   afterAsync?: ActionEventHandler;

  /**
   * 7) Middleware that decides whether an async mutator should be called 
   */
  execAsync?: ActionMiddleWareHandler;
  
  /**
   * 8) Respond to a middleware aborting that ran after you
   */
  abortAsyncMutator?: ActionEventHandler;

  /**
   * 9) Middleware finished and Async Mutator is about to run
   */
  beforeAsyncMutator?: ActionEventHandler;

  /**
   * 10) Model finished changing 
   */
  afterMutation?: ModelEventHandler;

  /**
   * 11) Action complete
   */
  after?: ActionEventHandler;
}

const MOUNT_KEY = 'MOUNT_KEY';
const CLASS_MOUNT_KEY = 'CLASS_MOUNT_KEY';
const STATE_KEY = 'STATE_KEY';
const DERIVED_STATE_KEY = 'DERIVED_STATE_KEY';
const ACTION_KEY = 'ACTION_KEY';
const ASYNC_ACTION_KEY = 'ASYNC_ACTION_KEY';
const NESTED_ACTION_KEY = 'NESTED_ACTION_KEY';
const STORE_INJECTION_KEY = 'STORE_INJECTION_KEY';

export const KERNEL = new Kernel();
export const STORE_KERNEL = new Kernel();

interface IPropInjectionMetadata { injectionId: string, propertyName: string };
interface IPropertyMountMetadata { middleware: (() => IMiddleware)[], propertyName: string };
interface IClassMountMetadata { middleware: (() => IMiddleware)[] };

//Middleware Mounting
export const mountProperty = parameterizedPropertyDecorator(MOUNT_KEY, (key, ...middleware) => {
	return { propertyName: key, middleware: middleware };
});

export const mountClass = parameterizedClassDecorator(CLASS_MOUNT_KEY, (...middleware) => {
	return { middleware: middleware };
});

export function mount(...args: (() => IMiddleware)[]): ClassDecorator & PropertyDecorator {
	return classAndPropertyDecorator(mountClass(...args), mountProperty(...args));
}

export const state = propertyDecorator(STATE_KEY, null, (target, key) => {
	observable.call(null, target, key);
});

// hooking up to event stream and making computed observable
export const derived_state = propertyDecorator(DERIVED_STATE_KEY, null, (target, key) => {
	computed.call(null, target, key);
});

export const action = propertyDecorator(ACTION_KEY);

export const async = propertyDecorator(ASYNC_ACTION_KEY);

export const nested = propertyDecorator(NESTED_ACTION_KEY);

export function middleware() : ClassDecorator {
	return function(target) {
		const middleware = <any>extendConstructor(target, function(middlewareProto) {
			const storesLoaded = wireStoreLoading(this, middlewareProto);

			extendObservable(this, { isLoading : true });	

			const depsFinishLoading = () => storesLoaded.get() === true;
			const stopLoading = () => this.isLoading = false;

			when(depsFinishLoading, stopLoading);	
		});

		return middleware;
	}
}

type ModelPropertyMap = {
	[propKey: string]: IMiddleware[]
}

type ActionPropertyMap = {
	[propKey: string]: {
		middlewareInstances: IMiddleware[];
		isAsync: boolean;
		isNested: boolean;
	}
}

export function store(id?: string | Symbol): ClassDecorator & PropertyDecorator {
	return classAndPropertyDecorator(createStore(), injectStore(id));
}

const injectStore = parameterizedPropertyDecorator(STORE_INJECTION_KEY, (key, id) => {
	return { injectionId: id, propertyName: key };
});

export const createStore = () => function<T extends TypedConstructorFunction<any>> (target: T) {
	const extendedStore = extendConstructor(target , async function(proto) {
		const loadingMiddleware = manageState(proto);

		extendObservable(this, { isLoading : true });	

		const middlewareLoaded = wireLoadingMiddleware(this, loadingMiddleware);
		const initialized = observable<boolean>(false);

		const depsFinishLoading = () => middlewareLoaded.get() === true && initialized.get() === true;
		const stopLoading = () => this.isLoading = false;

		when(depsFinishLoading, stopLoading);

		if (this.initialize) {
			await this.initialize();
		} 

		initialized.set(true);
	});

	decorate(injectable(), extendedStore)

	return extendedStore;
}

function manageState(proto): IMiddleware[] {
	let stateProperties: string[] = proto[STATE_KEY] || [],
		derivedStateProperties: string[] = proto[DERIVED_STATE_KEY] || [],
		actionProperties: string[] = proto[ACTION_KEY] || [],
		asyncActionProperties: string[] = proto[ASYNC_ACTION_KEY] || [],
		nestedActionProperties: string[] = proto[NESTED_ACTION_KEY] || [],
		classMiddlewareMetadata: IClassMountMetadata = proto[CLASS_MOUNT_KEY] || {},
		propertyMiddlewareMetadata: IPropertyMountMetadata[] = proto[MOUNT_KEY] || [],
		modelpropertyMap: ModelPropertyMap = {},
		actionPropertyMap: ActionPropertyMap = {};

	stateProperties.forEach(prop => modelpropertyMap[prop] = []);
	derivedStateProperties.forEach(prop => modelpropertyMap[prop] = []);
	
	actionProperties.forEach(prop => actionPropertyMap[prop] = { middlewareInstances: [], isAsync: false, isNested: false })
	asyncActionProperties.forEach(prop => actionPropertyMap[prop] && (actionPropertyMap[prop].isAsync = true));
	nestedActionProperties.forEach(prop => actionPropertyMap[prop] && (actionPropertyMap[prop].isNested = true));
	
	const loadingMiddleware = [];
	
	const classMiddleware = [];
	classMiddlewareMetadata.middleware && classMiddlewareMetadata.middleware.forEach(create => {
		const instance = create();

		if (instance.isLoading) {
			loadingMiddleware.push(instance);
		}

		classMiddleware.push(instance);
	});

	propertyMiddlewareMetadata && propertyMiddlewareMetadata.forEach(metadata => {
		if (modelpropertyMap[metadata.propertyName]) {
			
			const propertyMiddleware = []; 
			metadata.middleware && metadata.middleware.forEach(create => {
				const instance = create();

				if (instance.isLoading) {
					loadingMiddleware.push(instance);
				}

				propertyMiddleware.push(instance);
			});

			modelpropertyMap[metadata.propertyName] = classMiddleware.concat(propertyMiddleware);							
		} else if (actionPropertyMap[metadata.propertyName]) {
			
			const propertyMiddleware = []; 
			metadata.middleware && metadata.middleware.forEach(create => {
				const instance = create();

				if (instance.isLoading) {
					loadingMiddleware.push(instance);
				}

				propertyMiddleware.push(instance);
			});

			actionPropertyMap[metadata.propertyName].middlewareInstances = classMiddleware.concat(propertyMiddleware);						
		}
	});

	wireModel.call(this, stateProperties, derivedStateProperties, modelpropertyMap);
	wireActions.call(this, actionProperties, actionPropertyMap);

	return loadingMiddleware;
}


export const controller = () => function(target: new (...args: any[]) => Component<any, any>) {
	const controller = <any>extendConstructor(target, function(controllerProto) {
		const loadingMiddleware = manageState(controllerProto);

		extendObservable(this, { isLoading : true });	

		const middlewareLoaded = wireLoadingMiddleware(this, loadingMiddleware);
		const storesLoaded = wireStoreLoading(this, controllerProto);

		const depsFinishLoading = () => middlewareLoaded.get() === true && storesLoaded.get() === true;
		const stopLoading = () => this.isLoading = false;

		when(depsFinishLoading, stopLoading);		

		const originalRender = this.render;

		this.render = (...args) => {
			// allows your parent to force you to be in a loading state
			if(this.isLoading || this.props.isLoading) {
				return this.renderLoading.apply(this, args);
			} else {
				return originalRender.apply(this, args);
			}
		};
	});

	mobxObserver(controller);
	decorate(injectable(), controller);

	return controller;
}
///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                           Model                                           //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
function wireModel(stateProperties: string[], derivedStateProperties: string[], propertyMap: ModelPropertyMap) {

	stateProperties.forEach(property => {
		let middlewareInstances = propertyMap[property] || [],
			last = null;

		//if you don't have any middleware instances that are associated with you don't even setup the autorun
		if (middlewareInstances.length === 0) {
			return;
		}		

		autorun(() => {
			const current = this[property];

			const event = <IModelEvent> {
				storeName: this.storeId,
				propertyName: property,
				from: last,
				to: current,
				derived: false,
				timestamp: new Date().toISOString()
			};

			middlewareInstances.forEach(observe => observe.afterMutation && observe.afterMutation(event));
			
			last = current;
		});
	});

	derivedStateProperties.forEach(derivedProperty => {
		const middlewareInstances = propertyMap[derivedProperty] || [];
				
		//if you don't have any middleware instances that are associated with you don't even setup the autorun
		if (middlewareInstances.length === 0) {
			return;
		}	

		let last = null;

		autorun(() => {
			const current = this[derivedProperty];

			const event = <IModelEvent> {
				storeName: this.storeId,
				propertyName: derivedProperty,
				from: last,
				to: current,
				derived: true,
				timestamp: new Date().toISOString()
			};

			middlewareInstances.forEach(observe => observe.afterMutation && observe.afterMutation(event));
			
			last = current;
		});
	});
}

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                           Actions                                         //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

//HOW DOES THIS WORK WITH BEFORE ASYNC MIDDLEWARE?!?!?!
// how do you cancel a mutation in a nested model?
async function runNestedAction(action: Function, args: any[], actionName: string) {
    let currentStep = await mobxAction(`${actionName}`, action)(...args)

	if (!currentStep || typeof currentStep !== 'function') {
		return currentStep;
	}

    let stepResponse, stepLevel = 0; 

    while (true) {			
        stepResponse  = await mobxAction(`async::[${stepLevel}]${actionName}`, currentStep)()

        if (typeof stepResponse === 'function') {
            currentStep = stepResponse;
        } else {
            return stepResponse;
        }

		stepLevel++;
    }
}

function wireActions(actionProperties: string[], propertyMap: ActionPropertyMap) {
	actionProperties.forEach((actionName: string) => {
		const oldAction = this[actionName].bind(this);

		const { middlewareInstances, isAsync, isNested } = propertyMap[actionName];

		if (isAsync && isNested) {
			throw new Error(`Action '${actionName}' can't be both async and nested`);
		}

		//TODO: optomize this for no mounted middleware

		let runMiddleWare = async (middlewares: IMiddleware[], middlewareHandle, abortHandle, event): Promise<any> => {
				let abortIdx,
					abortReason = null,
					abort = (reason?) => { 
						abortReason = reason || 'aborted';
					}, 
					stack = [];

				for (let idx in middlewares) {
					let middleware = middlewares[idx];
					abortIdx = idx;

					let dispatchMiddleware : Function = middlewareHandle(middleware);

					if (!dispatchMiddleware) continue;

					await dispatchMiddleware.call(middleware, abort, event);
					stack.push(middleware);

					if (!!abortReason) {
						while(stack.length) {
							let abortMiddleware = stack.pop(),
								dispatchAbort = abortHandle(abortMiddleware);

							if (!dispatchAbort) continue;

							await dispatchAbort.call(abortMiddleware, event);
						}
						
						break;
					}
				}
				
				return abortReason;
			},
			startEvent = (event) => {
				middlewareInstances.forEach(observe => observe.start && observe.start(event));
			},
			execMiddleWare = (event) => {
				return runMiddleWare(middlewareInstances, (m) => m.exec, (m) => m.abort, event);
			},
			beforeMutatorEvent = (event) => {
				middlewareInstances.forEach(middleware => middleware.beforeMutator && middleware.beforeMutator(event));
			},
			beforeAsyncEvent = (event) => {
				middlewareInstances.forEach(middleware => middleware.beforeAsync && middleware.beforeAsync(event));
			},
			afterAsyncEvent = (event) => {
				middlewareInstances.forEach(middleware => middleware.afterAsync && middleware.afterAsync(event));
			},
			execAsyncMiddleware = (event) => {
				return runMiddleWare(middlewareInstances, (m) => m.execAsync, (m) => m.abortAsyncMutator, event);
			},
			beforeAsyncMutatorEvent = (event) => {
				middlewareInstances.forEach(middleware => middleware.beforeAsyncMutator && middleware.beforeAsyncMutator(event));
			},
			afterEvent = (event) => {
				middlewareInstances.forEach(middleware => middleware.after && middleware.after(event));
			};

		this[actionName] = (...args: any[]) => {
			// treat sync and async actions as async for consistency
			return new Promise(async (resolve, reject) => {
				let event = <IActionEvent> {
					storeName: this.storeId,
					actionName: actionName,
					args: args,
					timestamp: new Date().toISOString()
				}; 

				try {
					
					//EVENT: start 
					startEvent(event);

					//EVENT: exec + abort?
					const abortActionMiddlewareReason = await execMiddleWare(event);
					
					if (!!abortActionMiddlewareReason) {
						const error = new ActionError(ActionErrorType.Abort, event, abortActionMiddlewareReason);
						reject(error);

						return;
					}

					if (isAsync) {
						//EVENT: beforeAsync
						beforeAsyncEvent(event);

						//allow middleware to alter args
						const mutator = await oldAction.apply(null, event.args);

						//EVENT: afterAsync
						afterAsyncEvent(event);

						//EVENT: execAsync + abortAsyncMutator?
						const abortMutatorActionMiddlewareReason = await execAsyncMiddleware(event);
						
						if (!!abortMutatorActionMiddlewareReason) {
							const error = new ActionError(ActionErrorType.AbortMutator, event, abortMutatorActionMiddlewareReason);
							reject(error);
							
							return;
						}

						//EVENT: beforeAsyncMutator
						beforeAsyncMutatorEvent(event);

						mobxAction(`async::${actionName}`, mutator)();							

						//EVENT: after
						afterEvent(event);

						// actions should only change the state...access to state is granted through the model
						// Promise api for synchronizing multiple store calls
						resolve();

					} else if(isNested) {
						await runNestedAction(oldAction, event.args, actionName);

						//EVENT: after
						afterEvent(event);

						// actions should only change the state...access to state is granted through the model
						// Promise api for synchronizing multiple store calls
						resolve();
					} else {
						//EVENT: beforeMutator
						beforeMutatorEvent(event);

						//allow middleware to alter args
						mobxAction(actionName, oldAction)(...event.args);	

						//EVENT: after		
						afterEvent(event);

						// actions should only change the state...access to state is granted through the model
						// Promise api for synchronizing multiple store calls
						resolve();
					}
				} catch(e) {
					const error = new ActionError(ActionErrorType.Exception, event, e);
					reject(error);
				}
			});
		};
	});
}

export interface IActionError {
	fromAbort: boolean;
	fromAbortMutator: boolean;
	fromException: boolean;
	event: IActionEvent;
	cause: any;
}
export class ActionError implements IActionError {

	constructor(private type: ActionErrorType, public event: IActionEvent, public cause){}
    get fromAbort(): boolean {
		return this.type === ActionErrorType.Abort;
	};

    get fromAbortMutator(): boolean {
		return this.type === ActionErrorType.AbortMutator;
	};

    get fromException(): boolean {
		return this.type === ActionErrorType.Exception;		
	};
  }

  export enum ActionErrorType {
	  Abort,
	  AbortMutator,
	  Exception
  }

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                         Middleware + Store Loading                        //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
function wireStoreLoading(instance: { isLoadingStore: boolean }, proto): IObservableValue<boolean> {
	const storesToInject: IPropInjectionMetadata[] = proto[STORE_INJECTION_KEY] || [];
	const signal = observable<boolean>(false);

	if (storesToInject.length === 0) {
		signal.set(true);
		return signal;
	}

	const storesToLoadCount = observable(storesToInject.length);

	storesToInject.forEach(storeInjectionInfo => {
		const store = STORE_KERNEL.get(storeInjectionInfo.injectionId);

		//inject storeId for action/model events
		if (!store.storeId) {
			store.storeId = storeInjectionInfo.injectionId;
		}

		instance[storeInjectionInfo.propertyName] = store;

		when(() => store.isLoading === false, () => storesToLoadCount.set(storesToLoadCount.get() - 1));
	});

	when(() => storesToLoadCount.get() === 0, () => signal.set(true));

	return signal;
}

function wireLoadingMiddleware(instance, middleware: IMiddleware[]): IObservableValue<boolean> {
	const middlewareToLoadCount = observable(middleware.length);
	const signal = observable<boolean>(false);

	if (middleware.length === 0) {
		signal.set(true);
		return signal;
	}

	middleware.forEach(middleware => {
		when(() => middleware.isLoading === false, () => {
			middlewareToLoadCount.set(middlewareToLoadCount.get() - 1);
		});
	});

	when(() => middlewareToLoadCount.get() === 0, () => signal.set(true));

	return signal;
}