import * as React from 'react';
import { KERNEL } from './engine';

export function Inject<P>(props: P & { id: string }) {
	let Component = KERNEL.get(props.id) as (P) => JSX.Element;

	if (!Component) {
		return (
			<div>{`Failed to inject component with ID ${props.id}`}</div>
		);
	}

	return (
		<Component { ...props }/>
	);
}