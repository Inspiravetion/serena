import * as React from 'react';
import {Controller} from '../core/controller';
import { store, state, derived_state, controller, IStyleProps, SortFunction, sortByTransform, sortByKey, ClickEvent } from '../core';
import {Container, VerticalContainer, Section, VerticalSection, CenteredContent} from './container';
import {observable, IObservableValue, extendObservable} from 'mobx';

const GridStyles = {
  position: 'relative'
}

const GridHeaderStyles = {
  border: '1px solid #ccc',
  borderBottom: '2px solid #ccc',
  padding: '8px 2px',
  position: 'absolute',
  top: '0',
  left: '0',
  height: '48px',
  boxShadow: '#ccc 0px 2px 1px 0px'
}

const GridHeaderCellStyles = {
  padding: '2px 8px',
  textAlign: 'center'
}

const GridBodyStyles = {
  height: 'calc(100% - 48px)',
  position: 'relative',
  top: '48px',
  left: '0',
  overflowY: 'scroll'
}

const GridRowStyles = {
  borderTop: '1px solid #ccc',
  borderRight: '1px solid #ccc'
}

const GridCellStyles = {
  borderLeft: '1px solid #ccc',
  padding: '2px 8px'
}

const GridRowDetailsStyles = {
  borderLeft: '1px solid #ccc',
  borderRight: '1px solid #ccc',
  borderTop: '1px solid #ccc'
}

export interface IGridRowProps<T> extends IStyleProps {
  columns: IColumnConfig<T>[];
  rowId: (rowData: T) => string;
  row: T;
  onCellClick?: (event: ClickEvent, rowData: T, columnConfig: IColumnConfig<T>) => void;
  onDetailsClicked?: (event: ClickEvent, rowData: T) => void;
  detailsContent?: (rowData: T) => JSX.Element;
}

@controller()
class GridRow<T> extends React.Component<IGridRowProps<T>, void> {

  @state showDetails = false; 

  onCellClicked(rowData: T, columnConfig: IColumnConfig<T>): (event: ClickEvent) => void {
    return (event: ClickEvent) => {
      this.props.onCellClick && this.props.onCellClick(event, rowData, columnConfig);
      this.showDetails = this.props.detailsContent && !this.showDetails;
    }
  }

  onDetailsClicked(rowData: T): (event: ClickEvent) => void {
    return (event: ClickEvent) => {
      this.props.onDetailsClicked && this.props.onDetailsClicked(event, rowData);
      this.showDetails = this.props.detailsContent && !this.showDetails;
    }
  }

  cellContent(row: T, col: IColumnConfig<T>) {
    if (col.component) {
      return col.component(row);
    }

    if (typeof col.key === 'function') {
      return col.key(row);
    }

    let key: string = col.key;

    return row[key] || '';
  } 

  renderLoading() {
    return null;
  }

  render() {
    return (
      <div>
        {
          this.props.detailsContent && this.showDetails
            ? <VerticalSection style={GridRowDetailsStyles} onClick={this.onDetailsClicked(this.props.row)}>
                { this.props.detailsContent(this.props.row) }
              </VerticalSection>
            : <VerticalSection>
              <Container style={GridRowStyles} key={this.props.rowId(this.props.row)}>
                {
                  this.props.columns.map(col =>                         
                    <Section set style={GridCellStyles} onClick={this.onCellClicked(this.props.row, col)}
                      key={col.id}>
                        {
                          this.cellContent(this.props.row, col)
                        }
                    </Section>  
                  )
                }
              </Container>
            </VerticalSection>
        }
      </div>
    )
  }
}

export interface IColumnConfig<T> {
  id: string;
  key: (rowData: T) => string | keyof T;
  label: string;
  onHeaderClick?: (event: ClickEvent, columnConfig: IColumnConfig<T>) => void;
  component?: (rowData: T) => JSX.Element; // omitting this uses whatever is on T[key] as a string
  sort?: SortFunction<T>; 
}

export interface IGridProps<T> extends IStyleProps {
  columns: IColumnConfig<T>[];
  rows: T[];
  rowId: (rowData: T) => string;
  sortable?: boolean;
  onCellClick?: (event: ClickEvent, rowData: T, columnConfig: IColumnConfig<T>) => void;
  detailsContent?: (rowData: T) => JSX.Element;
  noRowsContent?: () => JSX.Element | string;
}

@controller()
export class Grid<T> extends React.Component<IGridProps<T>, void> {

  @state sortHandler: SortFunction<T> = sortByKey<T>();
  @state sortColumn: string = null;
  @state reverseSort: boolean = false;

  constructor(props) {
    super(props);
  }  

  //@memoize('id') memoize off key id
  onHeaderClicked(columnConfig: IColumnConfig<T>): (event: ClickEvent) => void {
    return (event: ClickEvent) => {
      if (this.props.sortable) {
        if (this.sortColumn === columnConfig.id) {
          this.reverseSort = !this.reverseSort;
        } else {
          let sortColumn = columnConfig.id,
              sortHandler = columnConfig.sort || typeof columnConfig.key === 'function' ? sortByTransform(columnConfig.key) : sortByKey<T>(columnConfig.key);

          this.sortColumn = sortColumn;
          this.sortHandler = sortHandler;
          this.reverseSort = false;
        }
      }
        
      columnConfig.onHeaderClick && columnConfig.onHeaderClick(event, columnConfig); 
    }
  }

  render() {
    if (!this.props.columns 
        || !this.props.columns.length) 
    {
      return <div>Developer Error</div>
    }

    const Row: new() => GridRow<T> = GridRow;

    const rows = this.sortColumn && this.sortHandler
      ? this.reverseSort
        ? this.props.rows.sort((a, b) => 0 - this.sortHandler(a, b))
        : this.props.rows.sort(this.sortHandler)
      : this.props.rows;

    return (
      <div id="gridRoot" style={Object.assign({}, this.props.style || {}, GridStyles)}>
        <Container style={GridHeaderStyles}>  
          { // column headers
            this.props.columns.map(col => 
              <Section set style={GridHeaderCellStyles} 
                  key={col.id} 
                  onClick={this.onHeaderClicked(col)}>
                { col.label }
              </Section>
            )
          }
        </Container>
        <VerticalContainer  style={GridBodyStyles}>
          { 
            rows && rows.length
            ? rows.map(row => {

                const rowProps: IGridRowProps<T> = {
                  row: row,
                  rowId: this.props.rowId,
                  columns: this.props.columns,
                  onCellClick: this.props.onCellClick,
                  detailsContent: this.props.detailsContent
                };

                return <Row { ...rowProps }/>
              })
            : (
                <VerticalSection>
                  { 
                    !this.props.noRowsContent || typeof this.props.noRowsContent === 'string'
                      ? <Container center={true}>
                          <span className='ms-Panel-headerText'>
                            { this.props.noRowsContent || 'No data to show.' }
                          </span>
                        </Container>
                      : this.props.noRowsContent()
                  }
                </VerticalSection>
              )
          }
        </VerticalContainer>
      </div>
    );
  }

  renderLoading() {
    return <div>Loading...</div>;
  }
}