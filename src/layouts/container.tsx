import * as React from 'react';
import {Controller} from '../core/controller';
import { store, state, controller, IStyleProps } from '../core';

const classnames = require('classnames/bind');
const styles = classnames.bind(require("./container.scss"));

/**
 * Base flex container that all other containers derive from
 */

interface IContainerProps extends IStyleProps {
  center?: boolean;
  full?: boolean;
  vertical?: boolean;
  relative?: boolean;
  absolute?: boolean;
  space_between?: boolean;
}

export const Container = (props: IContainerProps) => {
  
  let classname = styles({
    container: true,
    center: props.center,
    full: true,//props.full,
    vertical: props.vertical,
    relative: props.relative,
    absolute: props.absolute,
    'space-between': props.space_between
  });

  if(props.className) {
     classname += ` ${props.className}`
  }

  return (
    <div {...props} className={classname} style={props.style || {}}>
      { props.children }
    </div>
  );
}

/**
 * Base Vertical flex container
 */

export const VerticalContainer = (props: IContainerProps) => {
  return <Container vertical {...props} style={props.style || {}}/>
}

/**
 * Relative and Absolute styled flex containers
 */

export const RelativeContainer = (props: IContainerProps) => {
  return <Container relative {...props} style={props.style || {}}/>
}

export const RelativeVerticalContainer = (props: IContainerProps) => {
  return <VerticalContainer relative {...props} style={props.style || {}}/>
}

export const AbsoluteContainer = (props: IContainerProps) => {
  return <Container absolute {...props} style={props.style || {}}/>
}

export const AbsoluteVerticalContainer = (props: IContainerProps) => {
  return <VerticalContainer absolute {...props} style={props.style || {}}/>
}

/**
 * Sections (for layout inside containers)
 */

interface ISectionProps extends IReactProps, IStyleProps {
  size?: number; //used to determine section size relative to other sections in the container...must be > 1
  set?: boolean; /*||*/ fixed?: boolean;//take up just as much spase as you need and don't collapse when you run out of space
}

//all cells fit to content or relative sizes if given
export const Section = (props: ISectionProps) => {
  const classname = styles('section', {
    set: props.set
  }) + ` ${props.className || ''}`;

  let style: { flexGrow?: number } = {};

  if (props.size) {
    style.flexGrow = props.size;
  }

  return( 
    <div {...props} className={classname} style={Object.assign(style, props.style)}>
      { props.children }
    </div>
  );
}

//all cells fit to content or relative sizes if given
export const VerticalSection = (props: ISectionProps) => {
  const classname = styles('vertical-section', {
    fixed: props.fixed
  });

  let style: { flexGrow?: number } = {};

  if (props.size) {
    style.flexGrow = props.size;
  }

  return (
    <div {...props} className={classname} style={Object.assign(style, props.style)}>
      { props.children }
    </div>
  );
}

/**
 * Layout containers
 */

interface ICenteredContentProps extends IStyleProps {
  verticalStyles?: React.CSSProperties;
  horizontalStyles?: React.CSSProperties;
}

export const CenteredContent = (props: ICenteredContentProps) => {
  return (
    <RelativeContainer { ...props }>
      <AbsoluteVerticalContainer center={true} style={props.verticalStyles}>
        <RelativeContainer center={true} style={props.horizontalStyles}>
          { props.children }
        </RelativeContainer>
      </AbsoluteVerticalContainer>
    </RelativeContainer>
  );
}

// export const DrawerContent = (props: IProps) => {
//   return (
//   );
// }

// export const VerticalDrawerContent = (props: IProps) => {
//   return (
//   );
// }

// export const List = (props: IProps) => {
//   return (
//   );
// }

// export const = (props: IProps) => {
//   return (
//   );
// }

// containers
// sections
// layouts